# React kata middle

This is an exercise for assessing one's frontend programming skills.
The guidelines for this kata can be found at : https://github.com/planity/test_recrutement/tree/master/frontend_middle


## Installation

Install the project using npm : 
```bash
npm i
```

## Launching the app

Launch the app with npm : 
```bash
npm start
```