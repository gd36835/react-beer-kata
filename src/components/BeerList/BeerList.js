import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch, faTimes } from '@fortawesome/free-solid-svg-icons'
import { searchBeersPaginated } from '../../store/actions/beersActions'
import { addBeerToCart, removeBeerFromCart } from '../../store/actions/cartActions'
import BeerItem from "./BeerItem";

const ITEMS_PER_PAGE = 8;

// paginated list of beers
function BeerList(props) {
  const [page, setPage] = useState(1);
  const { addBeerToCart, removeBeerFromCart } = props
  const [searchInput, setSearchInput] = useState("");
  const [submittedQuery, setSubmittedQuery] = useState("");

  useEffect(() => {
    props.searchBeersPaginated(submittedQuery, page, ITEMS_PER_PAGE);
  }, [page, submittedQuery])

  async function submitSearch() {
    setPage(1);
    setSubmittedQuery(searchInput);
  }

  async function clearSearch() {
    setPage(1);
    setSearchInput('');
    setSubmittedQuery('');
  }

  if (props.loading) {
    return <div>Loading</div>;
  }

  return (
    <div className="section py-0">
      <div className="field is-grouped">
        <div className="control">
          <input type="text"
            value={searchInput}
            onChange={e => setSearchInput(e.target.value)}
            className="input is-info"
            placeholder="Type in your favorite beer" />
        </div>
        <p className="control">
          <button onClick={clearSearch} className="button is-light mr-3">
            <FontAwesomeIcon icon={faTimes} />
          </button>
          <button onClick={submitSearch} className="button is-info">
            <FontAwesomeIcon icon={faSearch} />
          </button>
        </p>
      </div>

      <div className="columns is-multiline">
        {props.beers.map(item => (
          <BeerItem
            key={item.id}
            item={item}
            addBeerToCart={addBeerToCart}
            removeBeerFromCart={removeBeerFromCart}
            isInCart={props.cart.some(b => b.id === item.id)}
          />
        ))}
      </div>
      <div className="columns is-mobile">
        <div className="column is-half">
          <button
            className='button is-pulled-right'
            disabled={page < 2}
            onClick={() => setPage(page - 1)}>Previous</button>
        </div>
        <div className="column is-half">
          <button
            disabled={props.beers.length < ITEMS_PER_PAGE}
            className='button'
            onClick={() => setPage(page + 1)}>Next</button>
        </div>
      </div>
    </div>
  );
};
const mapStateToProps = (state) => ({ beers: state.beers.beers, cart: state.cart.cart })

export default connect(mapStateToProps, { searchBeersPaginated, addBeerToCart, removeBeerFromCart })(BeerList)
