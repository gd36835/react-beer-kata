import React from "react"
import { Link } from 'react-router-dom';
import BeerImage from "./BeerImage"
import styles from './BeerItem.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCartPlus, faTimes } from '@fortawesome/free-solid-svg-icons'

// card with image and basic info about 1 beer
const BeerItem = (props) => {
  const { id, name, image_url, volume } = props.item
  const { isInCart } = props;

  function onAddToCart(e) {
    e.preventDefault();
    props.addBeerToCart(props.item)
  };

  function onRemoveFromCart(e) {
    e.preventDefault();
    props.removeBeerFromCart(props.item.id)
  };

  return (
    <div className="column is-3">
      <div className={`has-text-centered ${styles['beer-card']}`} style={{ height: "100%" }}>
        <Link to={`/beer/${id}`}>
          <div className="beer-card-image has-background-light" style={{ padding: 20 }}>
            <BeerImage image_url={image_url} />
          </div>
          <div className={`${styles["beer-card-content"]}`}>
            <h1 className="title is-4 is-capitalized">{name}</h1>
            <p className="subtitle is-6 mb-2">{volume.value} {volume.unit}</p>
            {isInCart ?
              <div className="columns is-vcentered is-mobile">
                <div className="column">
                  <span className="subtitle">In cart</span>
                </div>
                <div className="column is-narrow">
                  <button className="level-item button is-small is-light is-danger"
                    onClick={onRemoveFromCart}>
                    <FontAwesomeIcon icon={faTimes} />
                  </button>
                </div>
              </div>
              :
              <button className="button is-success is-small" onClick={onAddToCart}>
                <FontAwesomeIcon icon={faCartPlus} />
              </button>
            }
          </div>
        </Link >
      </div >
    </div >
  )
}

export default BeerItem;
