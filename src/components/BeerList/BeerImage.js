import React from 'react';

const BeerImage = ({ name, image_url, size = 150 }) => (
  <div>
    <img
      src={image_url}
      style={{ height: size }}
      alt={name}
    />
  </div>
);

export default BeerImage;
