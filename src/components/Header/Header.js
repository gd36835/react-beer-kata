import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'
import styles from './Header.module.css';

const Header = (props) => (
    <nav
        className="header level is-mobile"
        style={{
            background: 'linear-gradient(90deg, rgba(223,141,3,1) 0%, rgba(236,157,0,1) 26%, rgba(255,248,151,1) 100%)',
            padding: '10px 20px',
        }}
    >
        <Link to="/" className="level-left">
            <h1 className="title has-text-white ml-5" style={{ display: 'inline-block' }}>
                The Beer Palace
            </h1>
        </Link>
        <Link to="/cart" className="level-right">
            <span className={`${styles["has-badge"]}`}
                data-count={props.cartLength ? props.cartLength : null}>
                <button className={`button is-ghost `}
                    style={{ color: 'black' }}>
                    <FontAwesomeIcon icon={faShoppingCart} />
                </button>
            </span>
        </Link>
    </nav>
);

const mapStateToProps = (state) => ({ cartLength: state.cart.cart.length })

export default connect(mapStateToProps, {})(Header);