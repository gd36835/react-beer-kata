import React, { useEffect } from 'react';
import { connect } from 'react-redux'
import BeerImage from "../BeerList/BeerImage"
import { fetchBeer } from "../../store/actions/beersActions";
import { useParams } from "react-router-dom"
import { Link } from 'react-router-dom';

// details for the current beer (determined by the url parameter)
function BeerDetails(props) {
    const { id } = useParams()

    useEffect(() => {
        props.fetchBeer(id);
    }, [id])

    function isInCart() {
        return props.cart.some(b => b.id === parseInt(id));
    }

    return (
        <div className="section py-0 px-5">
            <Link to="/">
                <button style={{ backgroundColor: '#F6C101' }}
                    className="button mb-5">Back to List</button>
            </Link>
            <div className="card columns py-2">
                <div className="column is-3">
                    <div className="card-image has-background-light has-text-centered"
                        style={{ borderRadius: 10, padding: 20 }}>
                        <BeerImage image_url={props.currentBeer?.image_url} size="250px" />
                    </div>
                </div>
                <div className="card-content content">
                    <div className="columns is-vcentered">
                        <div className="column">
                            <h1 className="title is-2 is-capitalized mt-5">{props.currentBeer?.name}</h1>
                            <p className="subtitle is-5">{props.currentBeer?.volume.value} {props.currentBeer?.volume.unit}</p>
                        </div>

                        {isInCart() ?
                            <div className="column">
                                <span className="subtitle">In cart</span>
                            </div>
                            : ''
                        }
                    </div>

                    <p className="mt-6" style={{ fontStyle: 'italic' }}>{props.currentBeer?.description}</p>
                    <hr />

                    <p className="subtitle is-4 mt-4">Ingredients</p>
                    <div className="columns">
                        <div className="column is-third">
                            <span>Malt:</span>
                            <ul className='mt-1'>
                                {props.currentBeer?.ingredients.malt.map((m, index) =>
                                    <li key={index}>{m.name} : {m.amount.value} {m.amount.unit}</li>
                                )}
                            </ul>
                        </div>
                        <div className="column is-third">
                            <span>Hops:</span>
                            <ul className='mt-1'>
                                {props.currentBeer?.ingredients.hops.map((h, index) =>
                                    <li key={index}>{h.name} : {h.amount.value} {h.amount.unit}</li>
                                )}
                            </ul>
                        </div>
                        <div className="column is-third">
                            <span>Yeast:</span>
                            <p>{props.currentBeer?.ingredients.yeast}</p>

                        </div>
                    </div>

                    <blockquote className='mt-6'>
                        <span>Brewer's tips:</span>
                        <p>{props.currentBeer?.brewers_tips}</p>
                    </blockquote>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = (state) => ({ currentBeer: state.beers.currentBeer, cart: state.cart.cart })

export default connect(mapStateToProps, { fetchBeer })(BeerDetails);
