import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'

import BeerCartItem from "./BeerCartItem";
import { removeBeerFromCart } from '../../store/actions/cartActions';

// shopping cart
function BeerCart(props) {
    const { removeBeerFromCart } = props

    return (
        <div className="section py-0">
            <div className="level">
                <div className="level-left">
                    <div className="level-item"></div>
                    <Link to="/">
                        <button style={{ backgroundColor: '#F6C101' }}
                            className="button">Back to List</button>
                    </Link>
                </div>
                <div className="level-item">
                    <h1 className="title is-3">My cart</h1>
                </div>
            </div>

            <div className="">
                {props.cart.map(item => (
                    <BeerCartItem
                        key={item.id}
                        item={item}
                        removeBeerFromCart={removeBeerFromCart}
                    />
                ))}
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({ cart: state.cart.cart })

export default connect(mapStateToProps, { removeBeerFromCart })(BeerCart)
