import React from "react"
import { Link } from 'react-router-dom';
import BeerImage from "../BeerList/BeerImage"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'

// beer item in the shopping cart
const BeerItem = (props) => {
  const { id, name, image_url } = props.item

  return (
    <>
      <div className="level">
        <div className="level-left">
          <div className="level-item mx-5" style={{ width: '80px' }}>
            <BeerImage image_url={image_url} size={'80px'} />
          </div>
          <div className="level-item">
            <Link to={`/beer/${id}`}>
              <h1 className="title is-4 is-capitalized ">{name}</h1>
            </Link>
          </div>
        </div>
        <div className="level-right">
          <button className="level-item button is-light is-danger"
            onClick={() => props.removeBeerFromCart(id)}>
            <FontAwesomeIcon icon={faTimes} />
          </button>
        </div>
      </div>
      <hr />
    </>
  )
}

export default BeerItem;
