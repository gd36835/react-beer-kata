import { BEERS_LIST_FETCHED, BEERS_FETCHING_ERROR, CURRENT_BEER_FETCHED } from './types'
import axios from 'axios'

export const searchBeersPaginated = (searchQuery, page, per_page) => async dispatch => {
    axios.get(`https://api.punkapi.com/v2/beers`, {
        params: {
            ...searchQuery.length && { 'beer_name': searchQuery },
            page: page,
            per_page: per_page
        }
    })
        .then(res => {
            dispatch({
                type: BEERS_LIST_FETCHED,
                payload: res.data
            })
        })
        .catch(e => {
            dispatch({
                type: BEERS_FETCHING_ERROR,
                payload: console.log(e),
            })
        })
}

export const fetchBeer = (id) => async dispatch => {
    axios.get(`https://api.punkapi.com/v2/beers/${id}`)
        .then(res => {
            dispatch({
                type: CURRENT_BEER_FETCHED,
                payload: res.data
            })
        })
        .catch(e => {
            dispatch({
                type: BEERS_FETCHING_ERROR,
                payload: console.log(e),
            })
        })
}
