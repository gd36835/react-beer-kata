import { CART_ARTICLE_REMOVED, CART_ARTICLE_ADDED } from './types'

export const addBeerToCart = (beer) => dispatch => {
    dispatch({
        type: CART_ARTICLE_ADDED,
        payload: beer
    })
}

export const removeBeerFromCart = (id) => dispatch => {
    dispatch({
        type: CART_ARTICLE_REMOVED,
        payload: id
    })
}
