import { BEERS_LIST_FETCHED, CURRENT_BEER_FETCHED } from '../actions/types'


const initialState = {
    beers: [],
    currentBeer: null,
    loading: true
}


export default function beersReducer(state = initialState, action) {
    switch (action.type) {
        case BEERS_LIST_FETCHED:
            return {
                ...state,
                beers: action.payload,
                loading: false
            }
        case CURRENT_BEER_FETCHED:
            return {
                ...state,
                currentBeer: action.payload[0],
                loading: false
            }
        default: return state
    }
}