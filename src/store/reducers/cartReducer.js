import { CART_ARTICLE_REMOVED, CART_ARTICLE_ADDED } from '../actions/types'


const initialState = {
    cart: [],
}


export default function cartReducer(state = initialState, action) {
    switch (action.type) {
        case CART_ARTICLE_REMOVED:
            return {
                ...state,
                cart: state.cart.filter(b => b.id !== action.payload),
            }
        case CART_ARTICLE_ADDED:
            return {
                ...state,
                cart: [...state.cart.filter(b => b.id !== action.payload.id), action.payload],
            }
        default: return state
    }
}