import './App.css';
import BeerList from "./components/BeerList/BeerList"
import BeerDetails from "./components/BeerDetails/BeerDetails"
import Header from './components/Header/Header';
import { Route, BrowserRouter, Routes } from 'react-router-dom';
import BeerCart from "./components/BeerCart/BeerCart"

function App() {
  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route path="/" element={<BeerList />} />
        <Route path="/beer/:id" element={<BeerDetails />} />
        <Route path="/cart" element={<BeerCart />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
